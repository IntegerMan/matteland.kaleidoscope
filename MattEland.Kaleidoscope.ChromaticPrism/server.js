'use strict';
var http = require('http');
var nlp = require('compromise');

var port = process.env.PORT || 1337;

var parseText = function (input) {
  console.log('Parsing text ' + input);

  var parsed = nlp(input);

  return {
    sentences: parsed.sentences().data(),
    topics: parsed.topics().data(),
    nouns: parsed.nouns().data(),
    verbs: parsed.verbs().data(),
    people: parsed.people().data(),
    places: parsed.places().data(),
    organizations: parsed.organizations().data(),
    dates: parsed.dates().data(),
    values: parsed.values().data()
  };
};

http.createServer(function (req, res) {

  var body = [];

  req.on('error', (err) => {
    console.error(err);
  }).on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString();

    res.on('error', (err) => {
      console.error(err);
    });

    if (!body) {
      body = "POST a sentence to have the service analyze it.";
    }
    var result = parseText(body);

    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(result));

  });


}).listen(port);
