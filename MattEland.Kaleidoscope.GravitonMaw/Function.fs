module MattEland.Kaleidoscope.GravitonMaw

open System.Web.Http
open MattEland.Medium
open Microsoft.Azure.WebJobs
open Microsoft.AspNetCore.Mvc
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.AspNetCore.Http

let getQueryValueFromRequest (request:HttpRequest) name : string =
    (string) request.Query.[name]
    
let getArticleFromSlug (slug:string) =
    let parser: MediumParser = new MediumParser()
    let fetchTask = parser.GetArticleDetails slug
    fetchTask.Result

let isNullOrWhitespace (input:string): bool = System.String.IsNullOrEmpty input
    
let getWebResponse (slug: string): ActionResult =
    match slug with
    | "" | null -> BadRequestErrorMessageResult("The 'slug' query string parameter is required.") :> ActionResult
    | _ -> OkObjectResult(getArticleFromSlug slug) :> ActionResult        
    
[<FunctionName("Paragraphs")>]
let httpTriggerFunction ([<HttpTrigger(AuthorizationLevel.Function, "get", Route = null)>] req : HttpRequest): ActionResult =
    getQueryValueFromRequest req "slug" |> getWebResponse
