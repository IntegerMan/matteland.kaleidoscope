using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using MattEland.Medium;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MattEland.Kaleidoscope.EventHorizon
{
    public static class Function1
    {

        [FunctionName("Tag")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            string tagName = await GetTagNameFromRequest(req);

            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "dotnet";
            }

            var parser = new MediumParser();
            return new OkObjectResult(await parser.SearchTagAsync(tagName));
        }

        private static async Task<string> GetTagNameFromRequest(HttpRequest req)
        {
            if (req == null) throw new ArgumentNullException(nameof(req));

            string tag = req.Query["slug"];
            if (!string.IsNullOrWhiteSpace(tag)) return tag;

            return await new StreamReader(req.Body).ReadToEndAsync();
        }
    }
}
