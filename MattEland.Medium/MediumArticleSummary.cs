﻿using System.Collections.Generic;

namespace MattEland.Medium
{
    public class MediumArticleSummary
    {
        public MediumArticleSummary(dynamic value)
        {
            Id = value.id;
            VersionId = value.versionId;
            CreatorId = value.creatorId;
            CollectionId = value.homeCollectionId;
            Title = value.title;
            Subtitle = value.content?.subtitle;
            Description = value.content?.metaDescription;
            Language = value.detectedLanguage;
            Slug = value.slug;
            UniqueSlug = value.uniqueSlug;

            Tags = new List<string>();
            Topics = new List<string>();

            if (value.virtuals != null)
            {
                foreach (var tag in value.virtuals.tags)
                {
                    Tags.Add(tag.slug.Value);
                }

                foreach (var topic in value.virtuals.topics)
                {
                    Topics.Add(topic.slug.Value);
                }
            }
        }

        public string Id { get; set; }
        public string VersionId { get; set; }
        public string CreatorId { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string Slug { get; set; }
        public string UniqueSlug { get; set; }

        public string CollectionId { get; set; }
        public string Url { get; set; }

        public List<string> Tags { get; set; }
        public List<string> Topics { get; set; }
    }
}