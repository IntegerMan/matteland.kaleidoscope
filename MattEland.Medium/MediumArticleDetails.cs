using System.Collections.Generic;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;

namespace MattEland.Medium
{
    public class MediumArticleDetails : MediumArticleSummary
    {
        [NotNull, ItemNotNull]
        private readonly IList<string> _paragraphs = new List<string>();

        public MediumArticleDetails(JObject value) : base(value)
        {
            dynamic node = value;
            var paragraphsNode = node.content.bodyModel.paragraphs;

            foreach (var paraNode in paragraphsNode)
            {
                var text = paraNode.text?.ToString();
                if (string.IsNullOrWhiteSpace(text)) continue;
                
                _paragraphs.Add(text);
            }
        }

        [NotNull, ItemNotNull]
        public IEnumerable<string> Paragraphs => _paragraphs;
    }
}