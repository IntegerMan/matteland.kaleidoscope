﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MattEland.Medium
{
    public static class WebHelper
    {
        public static async Task<string> GetJsonAsync(Uri url)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync(url).ConfigureAwait(false);

                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        public static async Task<JObject> GetJObjectAsync(Uri url)
        {
            string json = await GetJsonAsync(url);

            json = PruneBeginningStringIfPresent(json, "])}while(1);</x>");

            return (JObject) JsonConvert.DeserializeObject(json);
        }

        private static string PruneBeginningStringIfPresent(string json, string search) 
            => json.StartsWith(search) ? json.Substring(search.Length) : json;
    }
}