﻿namespace MattEland.Medium
{
    public class MediumCollectionInfo
    {
        public MediumCollectionInfo(dynamic value)
        {
            Id = value.id;
            Name = value.name;
            Slug = value.slug;
            Description = value.description;
            ShortDescription = value.shortDescription;
            Domain = value.domain;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Domain { get; set; }
    }
}