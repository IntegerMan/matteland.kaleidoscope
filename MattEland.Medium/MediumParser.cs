﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace MattEland.Medium
{
    public class MediumParser
    {
        public async Task<IEnumerable<MediumArticleSummary>> SearchTagAsync(string tagName)
        {
            Uri uri = new Uri($"https://medium.com/tag/{HttpUtility.UrlEncode(tagName)}/archive/{DateTime.Today.Year}");

            var payload = await GetPayloadFromUri(uri);

            var collections = BuildCollectionList(payload);
            var articles = BuildArticleList(payload, collections);

            return articles;
        }

        private static async Task<dynamic> GetPayloadFromUri(Uri uri)
        {
            dynamic obj = await WebHelper.GetJObjectAsync(uri);

            if (obj.success != true)
            {
                throw new NotSupportedException($"The resource at {uri} returned a failure response.");
            }

            dynamic payload = obj.payload;
            return payload;
        }

        public async Task<MediumArticleDetails> GetArticleDetails(string uniqueSlug)
        {
            Uri uri = new Uri($"https://medium.com/p/{uniqueSlug}");

            var payload = await GetPayloadFromUri(uri);

            var article = new MediumArticleDetails(payload.value);
            
            return article;
        }

        private static IEnumerable<MediumArticleSummary> BuildArticleList(dynamic payload, 
                                                                          ISet<MediumCollectionInfo> collections)
        {
            var articles = new List<MediumArticleSummary>();
            JObject posts = payload.references.Post;

            if (posts != null)
            {
                foreach (var kvp in posts)
                {
                    var summary = new MediumArticleSummary(kvp.Value);
                    var collection = collections.FirstOrDefault(c => c.Id == summary.CollectionId);
                    if (collection != null)
                    {
                        summary.Url = $"{collection.Domain}/{summary.UniqueSlug}";
                    }

                    articles.Add(summary);
                }
            }

            return articles;
        }

        private static ISet<MediumCollectionInfo> BuildCollectionList(dynamic payload)
        {
            var collections = new HashSet<MediumCollectionInfo>();
            JObject posts = payload.references.Collection;

            if (posts != null)
            {
                foreach (var kvp in posts)
                {
                    var col = new MediumCollectionInfo(kvp.Value);

                    collections.Add(col);
                }
            }

            return collections
;
        }
    }
}